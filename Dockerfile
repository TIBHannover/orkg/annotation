FROM ubuntu:bionic

WORKDIR /app

# The official Python Docker images are based on buildpack-deps;
# if changed, build-essentials and git needs to be installed additionally.

# Install models. They are large and do not change much.
COPY models /app/models/

# Update requirements. If this file changes, everything else must be re-built.
COPY requirements.txt /app

# Install pip and java (needed for tabula), install git (needed for installing a python package from a git repo)
RUN apt-get -qqy update && \
    apt-get -qqy install python3-pip wget default-jre git dos2unix

# Install pdf2htmlEX
RUN wget -O pdf2html.deb https://github.com/pdf2htmlEX/pdf2htmlEX/releases/download/v0.18.8.rc1/pdf2htmlEX-0.18.8.rc1-master-20200630-Ubuntu-bionic-x86_64.deb && \
    apt-get -qqy install ./pdf2html.deb

# Install dependencies
RUN pip3 install --upgrade pip && \
    pip3 install --no-cache-dir Cython && \
    pip3 install python-dotenv
RUN pip3 install --no-cache -r requirements.txt
RUN pip install https://github.com/explosion/spacy-models/releases/download/en_core_web_md-2.1.0/en_core_web_md-2.1.0.tar.gz
#RUN python3 -m spacy download en_core_web_md

# Copy the rest. Prevent redundant copying of models.
#COPY . /app/
COPY annotator/                /app/annotator/
COPY ner/                      /app/ner/
COPY static                    /app/static/
COPY templates                 /app/templates/
COPY *.py                      /app/
# Include default values; override in deployment image
COPY .env                      /app/.env

RUN find /app/models/ -type f -print0 | xargs -0 dos2unix

EXPOSE 5000

CMD ["flask", "run", "--host=0.0.0.0"]
