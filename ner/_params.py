from marshmallow import Schema, validate
from webargs import fields

class LabelerGetParams(Schema):

    title = fields.String(required=True, validate=validate.Length(min=1))
    abstract = fields.String(required=True, validate=validate.Length(min=1))

    link_resources = fields.Boolean()    

