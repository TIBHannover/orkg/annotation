from flask import Blueprint
from .views import SequenceLabelerAPI

ner_blueprint = Blueprint("ner", __name__)

ner_blueprint.add_url_rule(
    "/sequenceLabeler/", view_func=SequenceLabelerAPI.as_view("sequence_labeler"), methods=["POST"]
)