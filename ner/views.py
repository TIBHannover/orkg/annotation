from flask import jsonify
from flask.views import MethodView
from ._params import ( LabelerGetParams )
from util import use_args_with
from ner import predict_orkg_contribution_entities

class SequenceLabelerAPI(MethodView):
    @use_args_with(LabelerGetParams)
    def post(self, reqargs):
        response = predict_orkg_contribution_entities.predict_orkg_contribution_entities(
            reqargs.get("title"),
            reqargs.get("abstract")
        )
        return jsonify(response)