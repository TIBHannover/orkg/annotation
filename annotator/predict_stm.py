import os
from collections import defaultdict
import time
import numpy as np
from allennlp.common import Params
from allennlp.data.token_indexers import PretrainedBertIndexer, TokenCharactersIndexer
from allennlp.models import Model
from util import STOP_WORDS
from .dataset_readers import TextDatasetReader


def load_bert_reader_and_model(experiment_dir: str, cuda_device: int = -1):
    # check values of existing config
    config_file = os.path.join(experiment_dir, "config.json")

    config = Params.from_file(config_file)
    # print(config["dataset_reader"].get("type"))
    config["dataset_reader"]["type"] = "text"
    # print(config["dataset_reader"].get("type"))
    path2scibert_scivocab = (
        os.environ["SCIBERT_SCIVOCAB"]
        if "SCIBERT_SCIVOCAB" in os.environ
        else "./models/scibert_scivocab_uncased/"
    )
    config["dataset_reader"]["token_indexers"]["bert"][
        "pretrained_model"
    ] = os.path.join(path2scibert_scivocab, "vocab.txt")
    config["model"]["text_field_embedder"]["token_embedders"]["bert"][
        "pretrained_model"
    ] = os.path.join(path2scibert_scivocab, "weights.tar.gz")

    # instantiate dataset reader
    reader = TextDatasetReader()
    reader.coding_scheme = config["dataset_reader"].get("coding_scheme")
    ind = Params.as_dict(config["dataset_reader"].get("token_indexers").get("bert"))
    token_indexer = PretrainedBertIndexer(
        ind.get("pretrained_model"),
        do_lowercase=bool(ind.get("do_lowercase")),
        use_starting_offsets=bool(ind.get("use_starting_offsets")),
    )
    ind = Params.as_dict(
        config["dataset_reader"].get("token_indexers").get("token_characters")
    )
    char_indexer = TokenCharactersIndexer(
        "characters", min_padding_length=int(ind.get("min_padding_length"))
    )
    reader._token_indexers.clear()
    # = {"bert": token_indexer, "token_characters", char_indexer}
    reader._token_indexers.__setitem__("bert", token_indexer)
    reader._token_indexers.__setitem__("token_characters", char_indexer)

    # instantiate model w/ pretrained weights
    model = Model.load(
        config.duplicate(),
        weights_file=os.path.join(experiment_dir, "best.th"),
        serialization_dir=experiment_dir,
        cuda_device=cuda_device,
    )

    # set training=false for prediction
    model.eval()

    return reader, model


path2model = (
    os.environ["BERT_MODEL_PATH"]
    if "BERT_MODEL_PATH" in os.environ
    else "./models/output/"
)
reader, model = load_bert_reader_and_model(experiment_dir=path2model)


def split_label(label_txt):
    if label_txt == "O":
        bio = label_txt
        label = label_txt
    else:
        bio, label = label_txt.split("-")
    return bio, label


# lambda function to convert an array of logit values to probabilities
def f(x):
    return np.exp(x) / (1 + np.exp(x))


def extract_spans_with_allennlp(instances, model, sentence_wise=True):
    s = time.time()

    all_extractions = defaultdict(list)

    # print('predicting data')
    predictions = model.forward_on_instances(instances)
    # print('predicting data finished')

    prev_id = ""
    for instance, prediction in zip(instances, predictions):
        id = "S00000"

        pred_tags = prediction["tags"]
        tokens = instance["tokens"]
        logits = prediction["logits"]

        # build extractions by converting BIO back to text w/ appropriate whitespace padding between merged tokens
        extractions = []
        inside_entity = False
        entity_confidence = 0.0
        n = 0
        prev_label = "O"
        predicted_labels = []

        for token, pred_tag, logit in zip(tokens, pred_tags, logits):

            bio, label = split_label(pred_tag)

            predicted_labels.append(label)

            # convert the logit array to a probability array
            logit_p = f(logit)
            # normalize the values of the probability array w.r.t. each other so that the values of the resulting array sum to 1
            logit_change = logit_p / np.sum(logit_p)
            # get the max normalized probability value which is score for the assigned tag
            prob = np.max(logit_change)

            # map BIOUL to BIO
            if bio == "U":
                bio = "B"
            elif bio == "L":
                bio = "I"

            if inside_entity:
                assert prev_label != "O"

                if bio == "O":
                    # entity ended
                    inside_entity = False
                    prev_label = label
                    entity_confidence = 0.0
                    n = 0

                elif prev_label != label:
                    # new entity
                    n = 1
                    entity_confidence = prob
                    extractions.append(
                        [
                            {
                                "entity": token.text,
                                "span": (token.idx, token.idx + len(token.text)),
                                "label": label,
                                "conf": entity_confidence / n,
                            }
                        ]
                    )
                    inside_entity = True
                    prev_label = label
                elif bio == "I":
                    # append inside entity
                    n += 1
                    entity_confidence += prob
                    extractions[-1].append(
                        {
                            "entity": token.text,
                            "span": (token.idx, token.idx + len(token.text)),
                            "label": label,
                            "conf": entity_confidence / n,
                        }
                    )
                    inside_entity = True
                    prev_label = label
                elif bio == "B":
                    # new entity
                    n = 1
                    entity_confidence = prob
                    extractions.append(
                        [
                            {
                                "entity": token.text,
                                "span": (token.idx, token.idx + len(token.text)),
                                "label": label,
                                "conf": entity_confidence / n,
                            }
                        ]
                    )
                    inside_entity = True
                    prev_label = label
            else:
                # outside of entity
                if bio in ["B", "I"]:
                    # new entity
                    n = 1
                    entity_confidence = prob
                    extractions.append(
                        [
                            {
                                "entity": token.text,
                                "span": (token.idx, token.idx + len(token.text)),
                                "label": label,
                                "conf": entity_confidence / n,
                            }
                        ]
                    )
                    inside_entity = True
                    prev_label = label
                else:
                    inside_entity = False
                    prev_label = label
                    entity_confidence = 0.0
                    n = 0

        all_extractions[id].extend(extractions)
        prev_id = id

    e = time.time()
    # print(f'Time took for prediction: {e - s} sec')
    return all_extractions, predicted_labels, (e - s)


def remove_stop_words(text, min_start):
    text_splitted = text.split(" ")
    if len(text_splitted) > 1:
        if text_splitted[0].lower() in STOP_WORDS:
            result = " ".join(text_splitted[1:])
            diff = len(text) - len(result)
            min_start = min_start + diff
            return remove_stop_words(result, min_start)
    return text, min_start


def predict_stm(text):

    text = text

    instances = reader.read(text)

    for ins in instances:
        ins["tags"].labels = ["O"] * len(ins["tags"].labels)
        """
        print(ins["tokens"])
        for token in ins["tokens"]:
            print(token.text+" "+str(token.idx))
        print(len(ins["tokens"]))
        print(ins["tags"])
        print(ins["metadata"])
        """

    all_extractions, token_predicted_labels, time = extract_spans_with_allennlp(
        instances, model
    )

    # each `extraction` is actually a List of tokens & metadata about that token
    # compile them into strings & metadata about that string
    for id, extractions in all_extractions.items():
        compiled_extractions = []
        for extraction in extractions:
            compiled_text = extraction[0]["entity"]
            min_start, max_stop = extraction[0]["span"]
            label = extraction[0]["label"]
            for d in extraction[1:]:
                assert d["label"] == label
                current_start, current_stop = d["span"]
                whitespace_between_tokens = " " * (current_start - max_stop)
                compiled_text += whitespace_between_tokens + d["entity"]
                max_stop = current_stop
            if len(extraction) == 1:
                continue
            compiled_text, min_start = remove_stop_words(compiled_text, min_start)
            compiled_extraction = {
                "entity": compiled_text,
                "span": (min_start, max_stop),
                "label": label,
                "confidence": d["conf"],
            }
            compiled_extractions.append(compiled_extraction)
        all_extractions[id] = compiled_extractions

    return all_extractions.items(), time


if __name__ == "__main__":
    # predict_stm function
    text = "This investigation examines the effect of manipulating soil microbial community composition and species richness on the development of soil structure over a seven month period in planted (with or without mycorrhizal fungi) and in unplanted macrocosms. The dilution method effectively resulted in soil communities with consistently contrasting levels of species (TRF) richness."
    all_extractions_items = predict_stm("text")
    # write in `.ann` format to match with semeval
    os.makedirs("./result/", exist_ok=True)
    for id, extractions in all_extractions_items:
        with open(
            os.path.join("./result/", f"{id}.ann"), "w", encoding="utf-8"
        ) as f_out:
            for i, extraction in enumerate(extractions):
                text = extraction["entity"]
                start, stop = extraction["span"]
                label = extraction["label"]
                confidence = extraction["confidence"]
                f_out.write(
                    "\t".join([f"T{i+1}", f"{label} {start} {stop}", f"{text}"])
                )
                f_out.write("\n")
