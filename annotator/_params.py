from marshmallow import Schema, validate
from webargs import fields

class LabelerGetParams(Schema):

    title = fields.String(required=True, validate=validate.Length(min=1))
    abstract = fields.String(required=True, validate=validate.Length(min=1))

    link_resources = fields.Boolean()    

class AnnotatorGetParams(Schema):

    text2annotate = fields.String(required=True, validate=validate.Length(min=1))

    link_resources = fields.Boolean()


class ExtractTableParams(Schema):
    pdf = fields.Field(required=True, location="files")
    page_number = fields.Int(required=True)
    region = fields.DelimitedList(fields.Str())
    lattice = fields.Bool(missing=False)


class ConvertPdfParams(Schema):
    pdf = fields.Field(required=True, location="files")


class ClassifySentenceParams(Schema):
    sentence = fields.String(required=True, validate=validate.Length(min=1))
    labels = fields.List(fields.Str(), required=True, validate=validate.Length(min=1))


class SummarizeTextParams(Schema):
    ratio = fields.Float(required=True)
