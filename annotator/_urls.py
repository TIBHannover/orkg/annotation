from flask import Blueprint
from .views import AnnotatorAPI, ExtractTable, ConvertPdf, ClassifySentence, SummarizeText


annotator_blueprint = Blueprint("annotator", __name__)

annotator_blueprint.add_url_rule(
    "/annotator/", view_func=AnnotatorAPI.as_view("annotate_text"), methods=["POST"]
)
annotator_blueprint.add_url_rule(
    "/extractTable/", view_func=ExtractTable.as_view("extract_table"), methods=["POST"]
)
annotator_blueprint.add_url_rule(
    "/convertPdf/", view_func=ConvertPdf.as_view("convert_pdf"), methods=["POST"]
)
annotator_blueprint.add_url_rule(
    "/classifySentence/", view_func=ClassifySentence.as_view("classify_sentence"), methods=["PUT"]
)
annotator_blueprint.add_url_rule(
    "/summarizeText/", view_func=SummarizeText.as_view("summarize_text"), methods=["PUT"]
)
