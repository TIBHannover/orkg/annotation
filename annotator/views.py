from flask import jsonify, request, redirect, url_for
from flask.views import MethodView
from ._params import (
    AnnotatorGetParams,
    ExtractTableParams,
    ConvertPdfParams,
    SummarizeTextParams,
    ClassifySentenceParams,
)
from util import use_args_with, get_certainty
from orkg import ORKG
from annotator import predict_stm
from transformers import pipeline
from summarizer import Summarizer
import os
import subprocess
import tabula
import tempfile
import shutil

host = (
    os.environ["ORKG_SERVER_URL"]
    if "ORKG_SERVER_URL" in os.environ
    else "https://www.orkg.org/orkg/"
)
orkg = ORKG(host)


def findResource(label=""):
    resources = orkg.resources.get(q=label, exact=True)
    if len(resources.content) > 0:
        return resources.content[0].get("id")
    return None

class AnnotatorAPI(MethodView):
    @use_args_with(AnnotatorGetParams)
    def post(self, reqargs):
        all_extractions_items, time = predict_stm.predict_stm(
            text=reqargs.get("text2annotate")
        )
        entities = []
        ac = [
            extraction["confidence"]
            for id, extractions in all_extractions_items
            for i, extraction in enumerate(extractions)
        ]
        for id, extractions in all_extractions_items:
            for i, extraction in enumerate(extractions):
                entities.append(
                    [
                        f"T{i+1}",
                        extraction["label"],
                        [extraction["span"]],
                        get_certainty(extraction["confidence"], ac),
                        findResource(extraction["entity"].strip())
                        if reqargs.get("link_resources")
                        else None,
                    ]
                )
        response = {
            "text": reqargs.get("text2annotate"),
            "entities": entities,
            "time": time,
        }
        return jsonify(response)


class ExtractTable(MethodView):
    @use_args_with(ExtractTableParams)
    def post(self, reqargs):
        # call tabula to parse the table
        data_frames = tabula.read_pdf(
            reqargs.get("pdf"),
            pages=reqargs.get("page_number"),
            lattice=reqargs.get("lattice"),
            pandas_options={
                "header": None
            },  # don't use headers since they need to be unique (pandas automatically as numbers to headers)
            area=reqargs.get("region"),
            multiple_tables=False,  # don't parse multiple tables
            encoding="utf-8",
        )

        return_data = []
        # only the first dataframe is returned
        if len(data_frames) > 0:
            return_data = data_frames[0].to_csv(
                encoding="utf-8", index=False, header=False
            )

        return jsonify(return_data)


class ConvertPdf(MethodView):
    DEFAULT_PDF_ZOOM = "1.33"

    @use_args_with(ConvertPdfParams)
    def post(self, reqargs):
        pdf = reqargs.get("pdf")

        # read the uploaded file contents
        content = pdf.stream.read()

        # save uploaded file as named temp file
        temp = tempfile.NamedTemporaryFile()
        temp.write(content)
        temp.seek(0)

        # make a temp dir for saving the parsed pdf (so the html)
        dirpath = tempfile.mkdtemp()
        output_file = "output.html"

        # run pdf2htmlEX
        html = subprocess.call(
            [
                "pdf2htmlEX",
                "--dest-dir",
                dirpath,
                "--zoom",
                ConvertPdf.DEFAULT_PDF_ZOOM,
                "--printing",
                "0",
                "--embed-outline",
                "0",
                temp.name,
                output_file,
            ],
            shell=False,
        )

        # open the created html file and return output as response
        f = open(dirpath + "/" + output_file, "r")
        html = f.read()

        # cleanup the temp dir
        shutil.rmtree(dirpath)

        return html


# ensure classifier is initialized only once (and not on each request)
classifier = pipeline("zero-shot-classification")
class ClassifySentence(MethodView):
    @use_args_with(ClassifySentenceParams)
    def put(self, reqargs):
        sentence = reqargs.get("sentence")
        labels = reqargs.get("labels")

        result = classifier(sentence, labels)

        return jsonify(result)


model = Summarizer()
class SummarizeText(MethodView):
    @use_args_with(SummarizeTextParams)
    def put(self, reqargs):
        body = request.data.decode("UTF-8")
        ratio = reqargs.get("ratio")
        result = model(body, ratio=ratio)

        return jsonify({"summary": result})
