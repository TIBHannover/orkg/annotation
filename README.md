# Services Offered

## 1] Computer Science Titles and Abstracts Annotation for Contribution Entities

### About

A sequence labeler that extracts scientific entities from either `Titles` or `Abstracts` of scholarly publications.
It extracts the following 7 types of semantic concepts from `Titles`: **_research problem_**, **_solution_**, **_resource_**, **_language_**, **_tool_**, **_method_** and **_dataset_**.
It extracts the following 2 types of semantic concepts from `Abstracts`: **_research problem_** and **_method_**.

### Details

Two models are developed. One on a dataset of only annotated `Titles` and a second on a dataset of annotated `Abstracts`.
For the `Titles` model, the underlying empirical dataset distributions were 25978/1564/10244 instances as Train/Dev/Test sets, respectively.
And for `Abstracts` model, the underlying empirical dataset distributions were 5958/441/1522 instances as Train/Dev/Test sets, respectively.

## 2] Annotation of Scholarly Abstracts across STEM (Science, Technology, Engineering, Medicine) disciplines

Annotation of abstracts service based on Flask.

Model maintainer : Dr. Jennifer D'Souza

Current annotation classes are : Process, Data, Material, Method

# API documentation

[The interactive API documentation can be found here.](https://gitlab.com/TIBHannover/orkg/annotation/-/blob/master/openapi.yml)

# Notes

-   The repository uses [Git LFS](https://git-lfs.github.com/) to track large files (e.g model), if you don't have it on your machine, you need to download those files manually:

    -   `models/output/best.th` <small>(513 MB)</small>
    -   `models/scibert_scivocab_uncased/weights.tar.gz` <small>(391 MB) </small>

# Setup

## Using docker compose

Run `docker-compose up -d` and you are ready to go. The application can be accessed via [http://localhost:8070/](http://localhost:8070/)

## Manual setup

### Requirements

Make sure you have [pdf2htmlEX](https://github.com/pdf2htmlEX/pdf2htmlEX/) installed. Additionally, you need to install [GROBID](https://github.com/kermitt2/grobid). But really, it is easier to use docker-compose to run this service. Every dependency is already included there.

-   Create your `.env` file, see `.env.example` for reference.

The **environment variables** , that you need to provide, are as follows:

| Variable         | Description                                                                                                                                |
| ---------------- | ------------------------------------------------------------------------------------------------------------------------------------------ |
| FLASK_APP        | Used to specify how to load the application for the 'flask' command.                                                                       |
| FLASK_ENV        | 'development' or 'production' and it's used to indicate to Flask, extensions, and other programs what context Flask is running in.         |
| FLASK_DEBUG      | Whether debug mode is enabled. **Do not enable debug mode when deploying in production.**                                                  |
| # ORKG API       |                                                                                                                                            |
| ORKG_SERVER_URL  | ORKG API endpoint                                                                                                                          |
| # Paths          |                                                                                                                                            |
| BERT_MODEL_PATH  | The path of bert model, this path should contain the files (config.json, best.th) and the folder (vocabulary). default: _./models/output/_ |
| SCIBERT_SCIVOCAB | The path of scibert*scibert files (weights.tar.gz, vocab.txt). default: *./models/scibert*scivocab_uncased/*                               |
| SCIBERT_SCIVOCAB | The path of scibert*scibert files (weights.tar.gz, vocab.txt). default: *./models/scibert*scivocab_uncased/*                               |
| LC_ALL           | To define local settings that the user uses. by default: `C.UTF-8`                                                                         |
| LANG             | To define which language the user uses. by default: `C.UTF-8`                                                                              |

-   Run `pip install -r requirements.txt`
-   Run `python -m spacy download en_core_web_md`
-   Run `python app.py` to launch the app.
-   The application can be accessed via [http://localhost:5000/](http://localhost:5000/) .

### Docker

-   Run `docker build . -t annotation` to build an image.
-   Run `docker run -d -p 5000:5000 annotation` to run the image.

### Contributing

-   We use [black](https://github.com/ambv/black) to format our code.

If you're using vscode, it's highly recommanded to adjust the project's `settings.json` file with something like:

```json
{
    "editor.formatOnSave": true,
    "python.formatting.provider": "black",
    "python.linting.lintOnSave": true,
    "python.formatting.blackPath": "/path/to/black"
}
```

This will auto format your code using `black`.
